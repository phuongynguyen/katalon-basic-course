import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://webdriveruniversity.com/Click-Buttons/index.html')

WebUI.click(findTestObject('Object Repository/ClickButtons/btn_ClickMeWebElement'))

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageWebElement'), 'Well done for successfully using the click() method!')

WebUI.click(findTestObject('Object Repository/ClickButtons/btn_CloseMessageWebElement'))

WebUI.click(findTestObject('Object Repository/ClickButtons/btn_ClickMeJavaScript'))

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageHeaderJavaScript'), 'It’s that Easy!! Well I think it is.....')

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageJavaScript'), 'We can use JavaScript code if all else fails! Remember always try to use the WebDriver Library method(s) first such as WebElement.click(). (The Selenium development team have spent allot of time developing WebDriver functions etc).')

WebUI.click(findTestObject('Object Repository/ClickButtons/btn_CloseMessageJavaScript'))

WebUI.click(findTestObject('Object Repository/ClickButtons/btn_ClickMeActionMove'))

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageHeaderActionMove'), 'Well done! the Action Move & Click can become very useful!')

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageActionMove'), 'Advanced user interactions (API) has been developed to enable you to perform more complex interactions such as:')

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageDrapDropActionMove'), 'Drag & Drop')

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageHoverClickActionMove'), 'Hover & Click')

WebUI.verifyElementText(findTestObject('Object Repository/ClickButtons/lbl_MessageClickHoldActionMove'), 'Click & Hold....')

WebUI.closeBrowser()